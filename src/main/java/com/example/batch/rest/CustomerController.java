package com.example.batch.rest;

import com.example.batch.service.BaseCustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private BaseCustomerService baseCustomerService;

    @GetMapping
    public void getBase() throws Exception {
        baseCustomerService.getCustomers();
    }
}
