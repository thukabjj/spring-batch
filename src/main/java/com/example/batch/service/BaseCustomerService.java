package com.example.batch.service;

import com.example.batch.config.mapping.CustomMappingStrategy;
import com.example.batch.domain.CustomerEntity;
import com.example.batch.repository.CustomerRepository;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BaseCustomerService {

    private final CustomerRepository customerRepository;


    public void getCustomers() throws Exception {
        List<CustomerEntity> all = customerRepository.findAll();
        String fileName = "C:\\Users\\arthur\\Documents\\testew\\baseCustomer.csv";

        try (FileOutputStream fos = new FileOutputStream(fileName);
             OutputStreamWriter osw = new OutputStreamWriter(fos,
                     StandardCharsets.UTF_8);

             CSVWriter writer = new CSVWriter(osw)) {

            CustomMappingStrategy<CustomerEntity> mappingStrategy = new CustomMappingStrategy<>();
            mappingStrategy.setType(CustomerEntity.class);
            StatefulBeanToCsv<CustomerEntity> statefulBeanToCsv = new StatefulBeanToCsvBuilder<CustomerEntity>(writer)
                    .withMappingStrategy(mappingStrategy)
                    .withApplyQuotesToAll(false)
                    .build();

            //write all users to csv file
            statefulBeanToCsv.write(all);
        }
    }
}
