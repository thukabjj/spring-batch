package com.example.batch.domain;


import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "customer")
public class CustomerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @CsvBindByName(column = "codigo_cliente")
    @CsvBindByPosition(position=0)
    private Long id;

    @Column(name = "first_name")
    @CsvBindByName(column = "nome")
    @CsvBindByPosition(position=1)
    private String firstName;

    @Column(name = "last_name")
    @CsvBindByName(column = "sobrenome")
    @CsvBindByPosition(position=2)
    private String lastName;

    @Column(name = "age")
    @CsvBindByName(column = "idade")
    @CsvBindByPosition(position=3)
    private Integer age;

    @Column(name = "identify")
    @CsvBindByName(column = "documento")
    @CsvBindByPosition(position=4)
    private String identify;

    @Column(name = "cellphone")
    @CsvBindByName(column = "celular")
    @CsvBindByPosition(position=5)
    private String cellphone;

}
