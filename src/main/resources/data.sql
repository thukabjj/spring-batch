
DROP TABLE IF EXISTS customer;

CREATE TABLE customer (
    id INT AUTO_INCREMENT  PRIMARY KEY,
    first_name    VARCHAR(250) NOT NULL,
    last_name     VARCHAR(250) NOT NULL,
    age           INT NOT NULL,
    identify      VARCHAR(11) DEFAULT NULL,
    cellphone     VARCHAR(11)
);
INSERT INTO customer (first_name,last_name,age,identify,cellphone) VALUES
('ARTHUR','COSTA',21,'12345678912','15987456321'),
('RAFAEL','SILVA',21,'65498732112','12345676358'),
('ITALO','HASHIMOTO',21,'78932145612','88885678912'),
('IAGO','YAMATSU',21,'25896314778','66665678912'),
('FERNANDA','SOUZA',21,'96325874131','98745678912');